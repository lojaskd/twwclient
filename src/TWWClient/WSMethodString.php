<?php
namespace TWWClient;

class WSMethodString{

    /** $__SOCKET_HEADER__
     * @access private
     * @var String $__SOCKET_HEADER__
    */
    private static $__SOCKET_HEADER__ = "POST %swsreluzcap.asmx HTTP/1.1\r\nHost: %s\r\nUser-Agent: PHP-LIBRARY\r\nContent-Type: text/xml; charset=utf-8\r\nContent-Length: %d\r\nSOAPAction: \"%s\"\r\nConnection: close\r\n\r\n%s\r\n\r\n";

    /** $ALTERA_SENHA
     * @access private
     * @var String $ALTERA_SENHA
    */
    private static $ALTERA_SENHA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><SOAP-ENV:Body><ns1:AlteraSenha><ns1:NumUsu><![CDATA[%s]]></ns1:NumUsu><ns1:SenhaAntiga><![CDATA[%s]]></ns1:SenhaAntiga><ns1:SenhaNova><![CDATA[%s]]></ns1:SenhaNova></ns1:AlteraSenha></SOAP-ENV:Body></SOAP-ENV:Envelope>";

    /** $BUSCA_SMS
     * @access private
     * @var String $BUSCA_SMS
    */
    private static $BUSCA_SMS =  "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><BuscaSMS xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><DataIni><![CDATA[%s]]></DataIni><DataFim><![CDATA[%s]]></DataFim></BuscaSMS></soap:Body></soap:Envelope>";

    /** $BUSCA_SMS_AGENDA
     * @access private
     * @var String $BUSCA_SMS_AGENDA
    */
    private static $BUSCA_SMS_AGENDA =  "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><BuscaSMSAgenda xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum></BuscaSMSAgenda></soap:Body></soap:Envelope>";

    /** $BUSCA_SMS_AGENDA_DATASET
     * @access private
     * @var String $BUSCA_SMS_AGENDA_DATASET
    */
    private static $BUSCA_SMS_AGENDA_DATASET = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><SOAP-ENV:Body><ns1:BuscaSMSAgendaDataSet><ns1:NumUsu><![CDATA[%s]]></ns1:NumUsu><ns1:Senha><![CDATA[%s]]></ns1:Senha><ns1:DS><xs:schema xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" id=\"OutDataSet\"><xs:element name=\"OutDataSet\" msdata:IsDataSet=\"true\" msdata:UseCurrentLocale=\"true\"><xs:complexType><xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\"><xs:element name=\"BuscaSMSAgenda\"><xs:complexType><xs:sequence><xs:element name=\"seunum\" type=\"xs:string\" minOccurs=\"0\"/></xs:sequence></xs:complexType></xs:element></xs:choice></xs:complexType></xs:element></xs:schema><diffgr:diffgram xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" xmlns:diffgr=\"urn:schemas-microsoft-com:xml-diffgram-v1\"><OutDataSet xmlns=\"\">%s </OutDataSet></diffgr:diffgram></ns1:DS></ns1:BuscaSMSAgendaDataSet></SOAP-ENV:Body></SOAP-ENV:Envelope>";

    /** $BUSCA_SMSMO
     * @access private
     * @var String $BUSCA_SMSMO
    */
    private static $BUSCA_SMSMO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><BuscaSMSMO xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><DataIni><![CDATA[%s]]></DataIni><DataFim><![CDATA[%s]]></DataFim></BuscaSMSMO></soap:Body></soap:Envelope>";

    /** $BUSCA_SMSMO_NAO_LIDO
     * @access private
     * @var String $BUSCA_SMSMO_NAO_LIDO
    */
    private static $BUSCA_SMSMO_NAO_LIDO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><BuscaSMSMONaoLido xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></BuscaSMSMONaoLido></soap:Body></soap:Envelope>";

    /** $DEL_SMS_AGENDA
     * @access private
     * @var String $DEL_SMS_AGENDA
    */
    private static $DEL_SMS_AGENDA = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><DelSMSAgenda xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><Agendamento><![CDATA[%s]]></Agendamento><SeuNum><![CDATA[%s]]></SeuNum></DelSMSAgenda></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS
     * @access private
     * @var String $ENVIA_SMS
    */
    private static $ENVIA_SMS = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMS xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem></EnviaSMS></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS2SN
     * @access private
     * @var String $ENVIA_SMS2SN
    */
    private static $ENVIA_SMS2SN = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMS2SN xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum1><![CDATA[%s]]></SeuNum1><SeuNum2><![CDATA[%s]]></SeuNum2><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem><Agendamento><![CDATA[%s]]></Agendamento></EnviaSMS2SN></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_AGE
     * @access private
     * @var String $ENVIA_SMS_AGE
    */
    private static $ENVIA_SMS_AGE ="<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSAge xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem><Agendamento><![CDATA[%s]]></Agendamento></EnviaSMSAge></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_AGE_QUEBRA
     * @access private
     * @var String $ENVIA_SMS_AGE_QUEBRA
    */
    private static $ENVIA_SMS_AGE_QUEBRA = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSAgeQuebra xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem><Agendamento><![CDATA[%s]]></Agendamento></EnviaSMSAgeQuebra></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_ALT
     * @access private
     * @var String $ENVIA_SMS_ALT
    */
    private static $ENVIA_SMS_ALT = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSAlt xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><user>%c</user><pwd>%c</pwd><msgid>%c</msgid><phone>%c</phone><msgtext>%c</msgtext></EnviaSMSAlt></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_CONCATENADO_COM_ACENTO
     * @access private
     * @var String $ENVIA_SMS_CONCATENADO_COM_ACENTO
    */
    private static $ENVIA_SMS_CONCATENADO_COM_ACENTO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSConcatenadoComAcento xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu>%s</NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Serie><![CDATA[%s]]></Serie><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem></EnviaSMSConcatenadoComAcento></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_CONCATENADO_SEM_ACENTO
     * @access private
     * @var String $ENVIA_SMS_CONCATENADO_SEM_ACENTO
    */
    private static $ENVIA_SMS_CONCATENADO_SEM_ACENTO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSConcatenadoSemAcento xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu>%s</NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Serie><![CDATA[%s]]></Serie><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem></EnviaSMSConcatenadoSemAcento></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_DATASET
     * @access private
     * @var String $ENVIA_SMS_DATASET
    */
    private static $ENVIA_SMS_DATASET = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSDataSet xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu>%s</NumUsu><Senha>%s</Senha><DS>%s</DS></EnviaSMSDataSet></soap:Body></soap:Envelope>";

    /** $ENVIA_SMSOTA_8BIT
     * @access private
     * @var String $ENVIA_SMSOTA_8BIT
    */
    private static $ENVIA_SMSOTA_8BIT = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSOTA8Bit xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Celular><![CDATA[%s]]></Celular><Header><![CDATA[%s]]></Header><Data><![CDATA[%s]]></Data></EnviaSMSOTA8Bit></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_QUEBRA
     * @access private
     * @var String $ENVIA_SMS_QUEBRA
    */
    private static $ENVIA_SMS_QUEBRA = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSQuebra xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum><Celular><![CDATA[%s]]></Celular><Mensagem><![CDATA[%s]]></Mensagem></EnviaSMSQuebra></soap:Body></soap:Envelope>";

    /** $ENVIA_SMS_TIM
     * @access private
     * @var String $ENVIA_SMS_TIM
    */
    private static $ENVIA_SMS_TIM = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><EnviaSMSTIM xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><XMLString>%s</XMLString></EnviaSMSTIM></soap12:Body></soap12:Envelope>";

    /** $ENVIA_SMS_XML
     * @access private
     * @var String $ENVIA_SMS_XML
    */
    private static $ENVIA_SMS_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><EnviaSMSXML xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><StrXML><![CDATA[%s]]></StrXML></EnviaSMSXML></soap:Body></soap:Envelope>";

    /** $InsBL
     * @access private
     * @var String $ENVIA_SMS_XML
    */
    private static $INS_BL_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><InsBL xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><Celular><![CDATA[%s]]></Celular></InsBL></soap:Body></soap:Envelope>";


    /** $RESETA_MO_LIDO
     * @access private
     * @var String $RESETA_MO_LIDO
    */
    private static $RESETA_MO_LIDO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ResetaMOLido xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></ResetaMOLido></soap:Body></soap:Envelope>";

    /** $RESETA_STATUS_SMS_NAO_LIDO
     * @access private
     * @var String $RESETA_STATUS_SMS_NAO_LIDO
    */
    private static $RESETA_STATUS_SMS_NAO_LIDO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ResetaStatusLido xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></ResetaStatusLido></soap:Body></soap:Envelope>";

    /** $STATUS_SMS
     * @access private
     * @var String $STATUS_SMS
    */
    private static $STATUS_SMS = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><StatusSMS xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum><![CDATA[%s]]></SeuNum></StatusSMS></soap:Body></soap:Envelope>";

    /** $STATUS_SMS2SN
     * @access private
     * @var String $STATUS_SMS2SN
    */
    private static $STATUS_SMS2SN = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><StatusSMS2SN xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><SeuNum1><![CDATA[%s]]></SeuNum1><SeuNum2><![CDATA[%s]]></SeuNum2></StatusSMS2SN></soap:Body></soap:Envelope>";

    /** $STATUS_SMS_DATASET
     * @access private
     * @var String $STATUS_SMS_DATASET
    */
    private static $STATUS_SMS_DATASET = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><StatusSMSDataSet xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha><DS><xs:schema xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" id=\"OutDataSet\"><xs:element name=\"OutDataSet\" msdata:IsDataSet=\"true\" msdata:UseCurrentLocale=\"true\"><xs:complexType><xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\"><xs:element name=\"StatusSMS\"><xs:complexType><xs:sequence><xs:element name=\"seunum\" type=\"xs:string\" minOccurs=\"0\"/></xs:sequence></xs:complexType></xs:element></xs:choice></xs:complexType></xs:element></xs:schema><diffgr:diffgram xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" xmlns:diffgr=\"urn:schemas-microsoft-com:xml-diffgram-v1\"><OutDataSet xmlns=\"\">%s</OutDataSet></diffgr:diffgram></DS></StatusSMSDataSet></soap:Body></soap:Envelope>";

    /** $STATUS_SMS_NAO_LIDO
     * @access private
     * @var String $STATUS_SMS_NAO_LIDO
    */
    private static $STATUS_SMS_NAO_LIDO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><StatusSMSNaoLido xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></StatusSMSNaoLido></soap:Body></soap:Envelope>";

    /** $VER_CREDITO
     * @access private
     * @var String $VER_CREDITO
    */
    private static $VER_CREDITO = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><VerCredito xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></VerCredito></soap:Body></soap:Envelope>";

    /** $VER_VALIDADE
     * @access private
     * @var String $VER_VALIDADE
    */
    private static $VER_VALIDADE = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><VerValidade xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></VerValidade></soap:Body></soap:Envelope>";

    /** $VER_BL
     * @access private
     * @var String $VER_BL
    */
    private static $VER_BL = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><VerBL xmlns=\"https://www.twwwireless.com.br/reluzcap/wsreluzcap\"><NumUsu><![CDATA[%s]]></NumUsu><Senha><![CDATA[%s]]></Senha></VerBL></soap:Body></soap:Envelope>";




    /** get_XML_SOCKET_HEADER__()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_SOCKET_HEADER__() {
        return self::$__SOCKET_HEADER__;
    }
    /** get_XML_ALTERA_SENHA()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ALTERA_SENHA() {
        return self::$ALTERA_SENHA;
    }
    /** get_XML_BUSCA_SMS()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_BUSCA_SMS() {
        return self::$BUSCA_SMS;
    }
    /** get_XML_BUSCA_SMS_AGENDA()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_BUSCA_SMS_AGENDA() {
        return self::$BUSCA_SMS_AGENDA;
    }
    /** get_XML_BUSCA_SMS_AGENDA_DATASET()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_BUSCA_SMS_AGENDA_DATASET() {
        return self::$BUSCA_SMS_AGENDA_DATASET;
    }
    /** get_XML_BUSCA_SMSMO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_BUSCA_SMSMO() {
        return self::$BUSCA_SMSMO;
    }
    /** get_XML_BUSCA_SMSMO_NAO_LIDO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_BUSCA_SMSMO_NAO_LIDO() {
        return self::$BUSCA_SMSMO_NAO_LIDO;
    }
    /** get_XML_DEL_SMS_AGENDA()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_DEL_SMS_AGENDA() {
        return self::$DEL_SMS_AGENDA;
    }
     /** get_XML_ENVIA_SMS()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS() {
        return self::$ENVIA_SMS;
    }
     /** get_XML_ENVIA_SMS2SN()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS2SN() {
        return self::$ENVIA_SMS2SN;
    }
     /** get_XML_ENVIA_SMS_AGE()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_AGE() {
        return self::$ENVIA_SMS_AGE;
    }
     /** get_XML_ENVIA_SMS_AGE_QUEBRA()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_AGE_QUEBRA() {
        return self::$ENVIA_SMS_AGE_QUEBRA;
    }
     /** get_XML_ENVIA_SMS_ALT()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_ALT() {
        return self::$ENVIA_SMS_ALT;
    }
     /** get_XML_ENVIA_SMS_CONCATENADO_COM_ACENTO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_CONCATENADO_COM_ACENTO() {
        return self::$ENVIA_SMS_CONCATENADO_COM_ACENTO;
    }
     /** get_XML_ENVIA_SMS_CONCATENADO_SEM_ACENTO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_CONCATENADO_SEM_ACENTO() {
        return self::$ENVIA_SMS_CONCATENADO_SEM_ACENTO;
    }
     /** get_XML_ENVIA_SMS_DATASET()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_DATASET() {
        return self::$ENVIA_SMS_DATASET;
    }
     /** get_XML_ENVIA_SMSOTA_8BIT()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMSOTA_8BIT() {
        return self::$ENVIA_SMSOTA_8BIT;
    }
     /** get_XML_ENVIA_SMS_QUEBRA()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_QUEBRA() {
        return self::$ENVIA_SMS_QUEBRA;
    }
     /** get_XML_ENVIA_SMS_TIM()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_TIM() {
        return self::$ENVIA_SMS_TIM;
    }
     /** get_XML_ENVIA_SMS_XML()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_ENVIA_SMS_XML() {
        return self::$ENVIA_SMS_XML;
    }

    /** get_XML_INS_BL()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_INS_BL() {
        return self::$INS_BL_XML;
    }
     /** get_XML_RESETA_MO_LIDO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_RESETA_MO_LIDO() {
        return self::$RESETA_MO_LIDO;
    }
     /** get_XML_RESETA_STATUS_SMS_NAO_LIDO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_RESETA_STATUS_SMS_NAO_LIDO() {
        return self::$RESETA_STATUS_SMS_NAO_LIDO;
    }
     /** get_XML_STATUS_SMS()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_STATUS_SMS() {
        return self::$STATUS_SMS;
    }
     /** get_XML_STATUS_SMS2SN()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_STATUS_SMS2SN() {
        return self::$STATUS_SMS2SN;
    }
     /** get_XML_STATUS_SMS_DATASET()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_STATUS_SMS_DATASET() {
        return self::$STATUS_SMS_DATASET;
    }
     /** get_XML_STATUS_SMS_NAO_LIDO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_STATUS_SMS_NAO_LIDO() {
        return self::$STATUS_SMS_NAO_LIDO;
    }
     /** get_XML_VER_CREDITO()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_VER_CREDITO() {
        return self::$VER_CREDITO;
    }
     /** get_XML_VER_VALIDADE()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_VER_VALIDADE() {
        return self::$VER_VALIDADE;
    }

    /** get_XML_VER_BL()
     * @access protected
     * @return String XML SOAP 1.1 VALUE.
    */
    protected static function get_XML_VER_BL() {
        return self::$VER_BL;
    }

}
